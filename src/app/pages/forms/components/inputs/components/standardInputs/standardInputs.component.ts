import {Component} from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
//import { UserData } from '../../providers/user-data';


@Component({
  selector: 'standard-inputs',
  templateUrl: './standardInputs.html',
})
export class StandardInputs {

  newPostings: FirebaseListObservable<any[]>;

  // newPost: FirebaseListObservable<any[]>;;
  // data: FirebaseListObservable<any[]>;
  theItems: FirebaseListObservable<any[]>;
  submitted = false;
  message: any;
  clientName: string;
  clientPhoneNumber: string;
  jobType: string;
  estimateDate: any;
  estimatePrice: number = 0;
  longDescription: string;
  timeSubmitted: String = new Date().toISOString();
  // data: {name?: string, text?: string} = any;

  showBallpark:boolean;
  
  ballpark: number = 0.0;
  
  changeShowStatus(){
    this.showBallpark = !this.showBallpark;
  }
  


  constructor() {
  
    this.showBallpark = true;
  }
  
  sendPost() {
      if(this.newPostings) {
          let Postings = {
              // from: this.uid,
              // message: this.message,
              // type: this.message,
              // year: this.year,
              clientName: this.clientName,
              clientPhoneNumber: this.clientPhoneNumber,
              jobType: this.jobType,
              estimateDate: this.estimateDate,
              estimatePrice: this.estimatePrice,
              longDescription: this.longDescription,
              timeSubmitted: this.timeSubmitted
          };
          this.newPostings.push(Postings);
          this.message = "";
      }
  };  
  
}
